FROM ruby:2.5-alpine as test
RUN apk add build-base libxml2-dev libxslt-dev tzdata nodejs sqlite sqlite-dev postgresql-client postgresql-dev
ENV RAILS_ENV=test
RUN mkdir /app
WORKDIR /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install
COPY . /app
RUN rails db:create db:migrate
RUN bundle exec rspec

FROM ruby:2.5-alpine
RUN apk add build-base libxml2-dev libxslt-dev tzdata nodejs sqlite sqlite-dev postgresql-client postgresql-dev
ENV RAILS_ENV=development
WORKDIR /app
ADD Gemfile /app/
ADD Gemfile.lock /app/
RUN bundle install
ADD . .
CMD ["puma"]
EXPOSE 3000

